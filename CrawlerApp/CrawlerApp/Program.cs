﻿using CrawlerApp.Process;

namespace CrawlerApp
{
    public class Program
    {
        /// <summary>
        /// Main
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            var processing = new Processing();
            processing.Run();
        }
    }
}
