﻿using System;

namespace CrawlerApp.Models
{
    public class MachineData
    {
        public Guid BotNameId { get; set; }
        public string OperatingSystem { get; set; }
    }
}
