﻿using System;

namespace CrawlerApp.Models
{
    public class Message
    {
        public Guid BotNameId { get; set; }
        public string InfoType { get; set; }
        public string Info { get; set; }
    }
}