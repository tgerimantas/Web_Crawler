﻿using System;
using System.Management;
using CrawlerApp.Models;

namespace CrawlerApp.Machine
{
    public class Machine
    {
        /// <summary>
        /// Gets mahine info
        /// </summary>
        /// <returns>machine info</returns>
        public static MachineInfo GetComputerInfo()
        {
            var machine = new MachineInfo();    
            var platform = GetPlatform();

            switch (platform)
            {
                case Platforms.Windows:
                    try
                    {
                        ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher("SELECT Caption FROM Win32_OperatingSystem");
                        foreach (var o in managementObjectSearcher.Get())
                        {
                            var managementObject = (ManagementObject)o;
                            machine.OperatingSystem = managementObject["Caption"].ToString();
                            break;
                        }
                    }
                    catch (Exception)
                    {
                        machine.OperatingSystem = "Windows";
                    }
                    break;
                case Platforms.Unix:
                    machine.OperatingSystem = "Linux";
                    break;
                case Platforms.NotDefined:
                    machine.OperatingSystem = "Not defined";
                    break;
                default:
                    throw new Exception("No machine info defined");
            }
            return machine;
        }

        /// <summary>
        /// Gets machine platform type
        /// </summary>
        /// <returns>platform</returns>
        public static Platforms GetPlatform()
        {
            var os = Environment.OSVersion;
            var pid = os.Platform;
            Platforms platform;

            switch (pid)
            {
                case PlatformID.Win32NT:
                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                case PlatformID.WinCE:
                    Console.WriteLine(Environment.OSVersion.Version);
                    Console.WriteLine(Environment.MachineName);
                    Console.WriteLine("Windows");
                    platform = Platforms.Windows;
                    break;                    
                case PlatformID.Unix:
                    Console.WriteLine("Unix");
                    platform = Platforms.Unix;
                    break;
                default:
                    Console.WriteLine("Not defined");
                    platform = Platforms.NotDefined;
                    break;
            }
            return platform;
        }
    }
}
