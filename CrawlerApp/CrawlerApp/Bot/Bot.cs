﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CrawlerApp.Process;
using CrawlerApp.Process.Logging;
using OpenQA.Selenium;

namespace CrawlerApp.Bot
{
    public class Bot<T>
    {       
        private readonly Browser<T> _browser;
        private IEnumerable<IWebElement> _links;
        private readonly Random _random;
        private readonly Guid _botNameId;
        private readonly ILogging _log;

        public Bot(T webDriver, ILogging log, Guid botNameId, string headUrl)
        {
            _botNameId = botNameId;
            _log = log;
            _random = new Random();

            //Get all url links from web page 
            _browser = new Browser<T>(webDriver);
            _browser.NavigatetoUrl(headUrl);
            _links = _browser.GetAllUrlLinks();
        }

        /// <summary>
        /// Runs bot commands
        /// </summary>
        public void Run()
        {
            while (true)
            {
                //Randomize how much steps will do
                var stepSize = _random.Next(1, 5);

                while (stepSize > 0)
                {
                    try
                    {
                        //Selects random web app command
                        ExecuteWebBrowser();
                        //Bot sleeps after executing command
                        Thread.Sleep(RandomThreadSleepTime());
                        stepSize--;
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.ToString());
                    }
                }

                //Takes a random url link to navigate
                var link = _links.ToList()[_random.Next(_links.ToArray().Length)];

                try
                {
                    var randomLink = link.GetAttribute("href");
                    var charList = randomLink.Take(4).ToArray();
                    var linkHeader = new string(charList);

                    if (linkHeader == "http")
                    {
                        _browser.NavigatetoUrl(randomLink);

                        _links = _browser.GetAllUrlLinks();

                        LogCommand(randomLink, "Navigate");

                        Thread.Sleep(RandomThreadSleepTime());
                    }
                }
                catch (Exception)
                {
                    _links = _browser.GetAllUrlLinks();
                    Thread.Sleep(RandomThreadSleepTime());
                }
            }
        }


        /// <summary>
        /// Selects random command
        /// </summary>
        public void ExecuteWebBrowser()
        {
            var randomAction = _random.Next(2, 12);

            switch (randomAction)
            {
                case 1: //Navigates to random url and gets all links
                    //int size = Links.ToArray().Length;
                    //string randomLink = _links.ToList()[_random.Next(size)].Text.ToString(); 
                    //string randomLink = Links.ToList()[Random.Next(size)].GetAttribute("href").ToString();
                    //Browser.NavigatetoUrl(randomLink);
                    //Links = Browser.GetAllUrlLinks();
                    //Browser.TakeScreenshot();
                    //Console.WriteLine("Navigate: "+randomLink);
                    //SendMessage(randomLink, "Navigate");  
                    break;
                case 2:
                    // _browser.NavigateBack();
                    //_browser.TakeScreenshot();
                    //Console.WriteLine("Press back ");
                    //SendMessage("Press back", "Navigate");      
                    LogCommand("Do nothing", "Navigate");
                    break;
                case 3:
                    _browser.ScrollDown(_random.Next(1, 200));
                    //Console.WriteLine("Jumps down");
                    LogCommand("Jumps down", "Scroll");
                    break;
                case 4:
                    _browser.ScrollUp(_random.Next(1, 200));
                    //Console.WriteLine("Jumps up");
                    LogCommand("Jumps up", "Scroll");
                    break;
                case 5:
                    for (int i = 0; i < _random.Next(1, 25); i++)
                    {
                        Thread.Sleep(40);
                        _browser.PressDown();
                    }
                    //Console.WriteLine("Scroll down by pressing button");
                    LogCommand("Scroll down", "Scroll");
                    break;
                case 6:
                    for (int i = 0; i < _random.Next(1, 25); i++)
                    {
                        Thread.Sleep(40);
                        _browser.PressUp();
                    }
                    //Console.WriteLine("Scroll up by pressing button");
                    LogCommand("Scroll up", "Scroll");
                    break;
                case 7:
                    _browser.ScrollToTheEnd();
                    //Console.WriteLine("Jump to the end");
                    LogCommand("Jump to the end", "Scroll");
                    break;
                case 8:
                    _browser.PageDown();
                    //Console.WriteLine("Page down");
                    LogCommand("Page down", "Scroll");
                    break;
                case 9:
                    _browser.PageUp();
                    //Console.WriteLine("Page up");
                    LogCommand("Scroll up", "Scroll");
                    break;
                case 10:
                    _browser.ButtonEnd();
                    //Console.WriteLine("Button end");
                    LogCommand("Button end", "Scroll");
                    break;
                case 11:
                    _browser.ButtonHome();
                    //Console.WriteLine("Button home");
                    LogCommand("Button home", "Scroll");
                    break;
            }
        }

        /// <summary>
        /// Random number generator for tread sleep
        /// </summary>
        /// <returns>random number</returns>
        private int RandomThreadSleepTime()
        {
            return _random.Next(2000, 7000);
        }

        /// <summary>
        /// Logs random commands
        /// </summary>
        /// <param name="command">command message</param>
        /// <param name="commandType">command type</param>
        private void LogCommand(string command, string commandType)
        {
            _log.SaveMessage(command, commandType);
        }
    }
}
