﻿using System;
using System.Configuration;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using CrawlerApp.Bot;
using CrawlerApp.Models;
using CrawlerApp.Process.Logging;
using Microsoft.AspNet.SignalR.Client;
using OpenQA.Selenium.Chrome;

namespace CrawlerApp.Process
{
    public class Processing
    {
        private static int _connectionMode = -1;
        private static ConsoleEventDelegate _handler;
        private static ILogging _logMode;

        /// <summary>
        /// Launch configured BOT
        /// </summary>
        public void Run()
        {
            var connectionUrl = ConfigurationManager.AppSettings.Get("ConnctionTestUrl");

            //New bot identification number
            var botNameId = Guid.NewGuid();
            //Returns machine info
            MachineInfo machineInfo = null;
            //Check if Chrome Web browser is installed in computer
            ChromeDriver webDriver = null;
        
            //If user presses x button on console window when bot is working, it sends a disconnect message to web app
            _handler = ConsoleEventCallback;
            SetConsoleCtrlHandler(_handler, true);

            try
            {
                machineInfo = Machine.Machine.GetComputerInfo();
                webDriver = new ChromeDriver();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Thread.Sleep(1000);
                Environment.Exit(0);
            }

            //Get a selected url that user selects
            var selectedUrl = GetSelectedUrl();

            //Initialize a connection between cloud server to log bot commands
            //If cant connect the program do offline mode - bot command logs to the generated file
            var connection = new HubConnection(connectionUrl);
            var monitorHub = connection.CreateHubProxy("MonitorHub");

            Console.WriteLine("Connecting to server...");

            try
            {
                connection.Start().Wait();
                _connectionMode = 1;
                _logMode = new HubLogging(botNameId, machineInfo, monitorHub);
                Console.WriteLine("Connected to server");
            }
            catch (Exception)
            {
                Console.WriteLine("Can't connect to server...");
                Console.WriteLine("Initialize offile mode");
                _connectionMode = 0;
                _logMode = new FileLogging(botNameId, machineInfo);
            }

            //Create a bot with configured data and execute in another thread
            var thread = new Thread(delegate()
            {
                try
                {
                    Console.WriteLine("Bot starts working");
                    new Bot<ChromeDriver>(webDriver, _logMode, botNameId, selectedUrl).Run();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e);
                    if (_connectionMode == 1)
                    {
                        _logMode.SaveMessage("Bot Error: " + e, "Error");
                        SendDisconnectMessage();
                    }
                }
            });

            thread.Start();


            Console.WriteLine("Press ESC to stop application");

            if (Console.ReadKey(true).Key == ConsoleKey.Escape)
            {
                if (_connectionMode == 1)
                {
                    _logMode.SaveMessage("Bot is shuting down", "Shut down");
                    SendDisconnectMessage();
                }

                Console.WriteLine("Bot is turning off");
                webDriver.Quit();
                Environment.Exit(0);
            }
        }

        /// <summary>
        /// Sends message  to inform a web app that selected bot is turning off
        /// </summary>
        private static void SendDisconnectMessage()
        {
            var mode = _logMode as IHubLogging;
            mode?.SendDisconnectMessage();
        }

        private static string GetSelectedUrl()
        {
            //Input head url
            var selectedUrl = "";
            var result = false;

            while (selectedUrl == "" || result == false)
            {
                Console.WriteLine("Please write a link");

                selectedUrl = Console.ReadLine();
                selectedUrl = "http://" + selectedUrl;
                Uri uriResult;
                result = Uri.TryCreate(selectedUrl, UriKind.Absolute, out uriResult)
                         && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            }

            return selectedUrl;
        }

        /// <summary>
        /// If user presses x button on console window when bot is working, it sends a disconnect message to web app
        /// </summary>
        /// <param name="eventType"></param>
        /// <returns></returns>
        private static bool ConsoleEventCallback(int eventType)
        {
            if (eventType == 2)
            {
                Console.WriteLine("Console window closing");
                if (_connectionMode == 1)
                {
                    _logMode.SaveMessage("Bot is shuting down", "Shut down");
                    SendDisconnectMessage();
                }
            }
            return false;
        }

        private delegate bool ConsoleEventDelegate(int eventType);
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetConsoleCtrlHandler(ConsoleEventDelegate callback, bool add);
    }
}
