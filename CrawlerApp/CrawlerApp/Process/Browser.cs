﻿using System;
using System.Collections.Generic;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;

namespace CrawlerApp.Process
{
    public class Browser<T>
    {
        private readonly IWebDriver _webDriver; 
        private readonly Actions _action;

        public Browser(T webDriver)
        {
            _webDriver = webDriver as IWebDriver;
            _action = new Actions(_webDriver);
        }

        //========================
        //Navigation

        /// <summary>
        /// Navigate browser to selected url
        /// </summary>
        /// <param name="url">web page url</param>
        public void NavigatetoUrl(string url)
        {
            _webDriver.Navigate().GoToUrl(url);
        }

        /// <summary>
        /// Navigate back
        /// </summary>
        public void NavigateBack()
        {
            ((IJavaScriptExecutor)_webDriver).ExecuteScript("history.go(-1);");
        }

        /// <summary>
        /// Maximize Web App
        /// </summary>
        public void MaximizeWebApplication()
        {
            _webDriver.Manage().Window.Maximize();
        }

        /// <summary>
        /// Returns all children WEB page urls
        /// </summary>
        /// <returns>links</returns>
        public ICollection<IWebElement> GetAllUrlLinks()
        {
            ICollection<IWebElement> links = _webDriver.FindElements(By.TagName("a"));
            return links;
        }

        //========================
        //Tabs

        /// <summary>
        /// Testing
        /// Opens a new tab and sets the context
        /// </summary>
        public void OpenNewBrowserTab() 
        {
            
            if (_webDriver is ChromeDriver)
            {
                //string script = String.Format("window.open('', 'tab{0}');", _webDriver.WindowHandles.Count);
                //_webDriver.ExecuteScript(script);
            }
            else if (_webDriver is FirefoxDriver) //Only works in firefox
            {
                //Actions action = new Actions(_webDriver);
                //action.SendKeys(OpenQA.Selenium.Keys.Control + "t").Build().Perform();
                IWebElement body = _webDriver.FindElement(By.TagName("body"));
                body.SendKeys(Keys.Control + "t");
            }
            NavigateToBrowserTab(_webDriver.WindowHandles.Count - 1); //Changes a tab handle
            Thread.Sleep(1000);
        }

        /// <summary>
        /// Change browser Tab
        /// </summary>
        /// <param name="redirectTabId">tab number</param>
        public void ChangeBrowserTab(int redirectTabId)
        {
            if (_webDriver is FirefoxDriver)
            {
                _action.SendKeys(Keys.Control + "1").Build().Perform(); //Only works in firefox
            }
            else if (_webDriver is ChromeDriver)
            {
                _webDriver.FindElement(By.CssSelector("body")).SendKeys(Keys.Control + "" + redirectTabId);
            }            
        }

        /// <summary>
        /// Changes a tab handle
        /// </summary>
        /// <param name="redirectTabId">tab number</param>
        public void NavigateToBrowserTab(int redirectTabId) 
        {
            Thread.Sleep(1000);
            //var handles = _webDriver.CurrentWindowHandle;
            _webDriver.SwitchTo().Window(_webDriver.WindowHandles[redirectTabId]);
        }

        /// <summary>
        /// Closes a web browser tab
        /// </summary>
        /// <param name="removeTabId">tab number to close</param>
        /// <param name="redirectTabId">tab number to redirect</param>
        public void CloseBrowserTab(int removeTabId, int redirectTabId)
        {
            _webDriver.SwitchTo().Window(_webDriver.WindowHandles[removeTabId]).Close();
            NavigateToBrowserTab(redirectTabId);
        }


        //========================
        //Logging

        public void TakeScreenshot() //Creates screenshots and save them to folder 
        {
            try
            {
                var url =
                    $"C:\\Screenshots\\SeleniumTestingScreenshot_{Convert.ToDateTime(DateTime.Now):yyyy_MM_dd_HH_mm_ss}.jpg";
                var screenshot = ((ITakesScreenshot)_webDriver).GetScreenshot();
                screenshot.SaveAsFile(url, ScreenshotImageFormat.Jpeg);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        //========================
        //Scrolling

        /// <summary>
        /// Pressing arrow down
        /// </summary>
        public void PressDown()
        {      
            _action.SendKeys(Keys.ArrowDown).Build().Perform();
        }

        /// <summary>
        /// Pressing arrow up
        /// </summary>
        public void PressUp()
        {       
            _action.SendKeys(Keys.ArrowUp).Build().Perform();
        }

        /// <summary>
        /// Jumps (scrolls) down using pixels 
        /// </summary>
        /// <param name="pixels"></param>
        public void ScrollDown(int pixels)
        {
            ((IJavaScriptExecutor)_webDriver).ExecuteScript($"window.scrollBy(0, {pixels});");
        }

        /// <summary>
        /// Jumps (scrolls) up using pixels 
        /// </summary>
        /// <param name="pixels"></param>
        public void ScrollUp(int pixels)
        {
            ((IJavaScriptExecutor)_webDriver).ExecuteScript($"window.scrollBy(0, {-pixels});");
        }

        /// <summary>
        /// Jumps (scrolls) to end of web page
        /// </summary>
        public void ScrollToTheEnd()
        {
            ((IJavaScriptExecutor)_webDriver).ExecuteScript("window.scrollTo(0, document.body.scrollHeight - 100)");
        }

        /// <summary>
        /// Pressing page up button
        /// </summary>
        public void PageUp()
        {
            _action.SendKeys(Keys.PageUp).Build().Perform();
        }

        /// <summary>
        /// Pressing page down button
        /// </summary>
        public void PageDown() 
        {
            _action.SendKeys(Keys.PageDown).Build().Perform();
        }

        /// <summary>
        /// Pressing home button
        /// </summary>
        public void ButtonHome()
        {
            _action.SendKeys(Keys.Home).Build().Perform();
        }

        /// <summary>
        /// Pressing end button
        /// </summary>
        public void ButtonEnd()
        {
            _action.SendKeys(Keys.End).Build().Perform();
        }
    }
}
