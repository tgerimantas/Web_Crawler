﻿namespace CrawlerApp.Process.Logging
{
    public interface IHubLogging : ILogging
    {
        void SendDisconnectMessage();
    }
}