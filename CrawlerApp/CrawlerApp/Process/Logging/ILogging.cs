﻿using System;

namespace CrawlerApp.Process.Logging
{
    public interface ILogging
    {
        void SaveMessage(string info, string infoType);
    }
}