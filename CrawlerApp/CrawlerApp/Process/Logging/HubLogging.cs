﻿using System;
using CrawlerApp.Models;
using Microsoft.AspNet.SignalR.Client;

namespace CrawlerApp.Process.Logging
{
    public class HubLogging : IHubLogging
    {
        private readonly Guid _botNameId;
        private readonly IHubProxy _monitorHub;

        public HubLogging(Guid botNameId, MachineInfo machineInfo, IHubProxy monitorHub)
        {
            _botNameId = botNameId;
            _monitorHub = monitorHub;
            BotRegistration(machineInfo);        
        }

        /// <summary>
        /// Register bot in server using SignalR
        /// </summary>
        /// <param name="machineInfo"></param>
        private void BotRegistration(MachineInfo machineInfo)
        {
            _monitorHub.Invoke<MachineData>("registration", new MachineData() { BotNameId = _botNameId, OperatingSystem = machineInfo.OperatingSystem });
        }

        /// <summary>
        /// Send message
        /// </summary>
        /// <param name="info">command text</param>
        /// <param name="infoType">command type</param>
        public void SaveMessage(string info, string infoType)
        {
            _monitorHub.Invoke<Message>("send", new Message() { Info = info, InfoType = infoType, BotNameId = _botNameId });
        }

        /// <summary>
        /// Sends bot disconnection message to inform web app
        /// </summary>
        public void SendDisconnectMessage()
        {
            _monitorHub.Invoke<MachineData>("disconnect", new MachineData() { BotNameId = _botNameId, OperatingSystem = null });
        }
    }
}