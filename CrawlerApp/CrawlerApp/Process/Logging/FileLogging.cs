﻿using System;
using System.IO;
using CrawlerApp.Models;

namespace CrawlerApp.Process.Logging
{
    public class FileLogging : ILogging
    {
        private readonly Guid _botNameId;
        private static string _fileName = string.Empty; 

        public FileLogging(Guid botNameId, MachineInfo machineInfo)
        {
            _botNameId = botNameId;
            var creationDate = DateTime.Now;
            _fileName = creationDate.ToString("yyyy_MM_dd_h_mm_ss")+".txt";
            AddFileHeader(machineInfo, creationDate);
        }

        /// <summary>
        /// Add header to logging file
        /// </summary>
        /// <param name="machineInfo">machine data</param>
        /// <param name="creationDate">date</param>
        private void AddFileHeader(MachineInfo machineInfo, DateTime creationDate)
        {
            var header = "Bot monitoring log : Name : " + _botNameId + "Date:" + creationDate + " Platform: " + machineInfo.OperatingSystem + "\n";
            File.AppendAllText(_fileName, header);
        }


        /// <summary>
        /// Save message to file
        /// </summary>
        /// <param name="commandMessage">command message</param>
        /// <param name="commandType">command type</param>
        public void SaveMessage(string commandMessage, string commandType)
        {
            var log = "Log Entry :" + DateTime.Now + " : " + commandMessage + ": command type :" + commandType + "\n";
            Console.WriteLine(log);
            File.AppendAllText(_fileName, log);
        }
    }
}
