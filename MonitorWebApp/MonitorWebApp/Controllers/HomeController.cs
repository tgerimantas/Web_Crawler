﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using Database = MonitorWebApp.Models.Database;

namespace MonitorWebApp.Controllers
{
    [RoutePrefix("Home")]
    public class HomeController : Controller
    {
        
        // GET: Home
        [HttpGet]    
        [Route("Index")]
        [Route("~/")]
        [Route("")]
        public ActionResult Index()
        {
            return View();
        }

        //GET :Download
        [HttpGet]
        [Route("Downloads")]
        public ActionResult Downloads()
        {
            return View();
        }


        //GET :Download
        [HttpGet]
        [Route("Monitor")]
        [OutputCache(Duration = 0, VaryByParam = "none", Location = OutputCacheLocation.Client, NoStore = true)]
        public async Task<ActionResult> Monitor()
        {
            using (var db = new Database())
            {
                var bots = await db.Bots.Where(t => t.StatusTypeId == 1).ToArrayAsync();
                ViewBag.bots = bots; 

                return View();
            }         
        }

        [HttpGet]
        public async Task<ActionResult> MonitorSelectedBot(int id)
        {
            using (var db = new Database())
            {
                var bot = await db.Bots.FirstOrDefaultAsync(t => t.Id == id);

                if (bot != null)
                {
                    ViewBag.MonitoringId = id;
                    ViewBag.BotNameId = bot.BotNameId;
                    ViewBag.CreateDate = bot.CreateDate;
                    ViewBag.Platform = bot.OperatingSystem;

                    return View();
                }

                return HttpNotFound();
            }
            
        }

        [AjaxOnly]
        public async Task<JsonResult> GetBots()
        {
            using (var db = new Database())
            {
                var bots = await db.Bots.ToListAsync();

                return Json(bots, JsonRequestBehavior.AllowGet);
            }
        }
    }
}