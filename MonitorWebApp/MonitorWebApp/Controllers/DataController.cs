﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Database = MonitorWebApp.Models.Database;

namespace MonitorWebApp.Controllers
{
  
    [RoutePrefix("Data")]
    public class DataController : Controller
    {
       
        [HttpGet]
        [Route("")]
        [Route("BotHistory")]
        public async Task<ActionResult> BotHistory()
        {
            using (var db = new Database())
            {
                var bots = await db.Bots.Where(t => t.StatusTypeId == 2).ToArrayAsync();
                ViewBag.Bots = bots;
            
                return View();
            }
        }

        public async Task<ActionResult> SelectedBotHistory(int id)
        {
            using (var db = new Database())
            {
                var bot = await db.Bots.Where(t => t.Id == id).FirstOrDefaultAsync();

                if (bot != null)
                {
                    ViewBag.BotId = id;
                    ViewBag.CreateDate = bot.CreateDate;
                    ViewBag.FinishDate = bot.FinishDate;
                    ViewBag.Platform = bot.OperatingSystem;
                    ViewBag.MonitoringData = await db.Monitor.Where(t => t.BotId == bot.Id).ToArrayAsync();

                    return View();
                }

                return HttpNotFound();
            }
        }


        public async Task<ActionResult> RemoveSelected(int id)
        {
            using (var db = new Database())
            {
                var selectedBot = await db.Bots.Where(t => t.Id == id).FirstOrDefaultAsync();

                if (selectedBot != null)
                {
                   db.Bots.Remove(selectedBot);

                   await db.SaveChangesAsync();

                   return RedirectToAction("BotHistory", "Data");
                }

                return HttpNotFound();
            }
        }
    }
}