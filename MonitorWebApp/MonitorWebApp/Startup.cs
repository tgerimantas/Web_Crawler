﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(MonitorWebApp.Startup))]
namespace MonitorWebApp
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}