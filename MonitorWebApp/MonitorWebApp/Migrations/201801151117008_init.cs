namespace MonitorWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bots",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BotNameId = c.String(),
                        CreateDate = c.String(),
                        FinishDate = c.String(),
                        OperatingSystem = c.String(),
                        StatusTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StatusTypes", t => t.StatusTypeId, cascadeDelete: true)
                .Index(t => t.StatusTypeId);
            
            CreateTable(
                "dbo.StatusTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Monitors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BotId = c.Int(nullable: false),
                        MessageDate = c.String(),
                        InfoType = c.String(),
                        Info = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Bots", t => t.BotId, cascadeDelete: true)
                .Index(t => t.BotId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Monitors", "BotId", "dbo.Bots");
            DropForeignKey("dbo.Bots", "StatusTypeId", "dbo.StatusTypes");
            DropIndex("dbo.Monitors", new[] { "BotId" });
            DropIndex("dbo.Bots", new[] { "StatusTypeId" });
            DropTable("dbo.Monitors");
            DropTable("dbo.StatusTypes");
            DropTable("dbo.Bots");
        }
    }
}
