namespace MonitorWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Bots", "CreateDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Bots", "FinishDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Monitors", "MessageDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Monitors", "MessageDate", c => c.String());
            AlterColumn("dbo.Bots", "FinishDate", c => c.String());
            AlterColumn("dbo.Bots", "CreateDate", c => c.String());
        }
    }
}
