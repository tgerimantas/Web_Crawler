namespace MonitorWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Bots", "CreateDate", c => c.DateTime());
            AlterColumn("dbo.Bots", "FinishDate", c => c.DateTime());
            AlterColumn("dbo.Monitors", "MessageDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Monitors", "MessageDate", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.Bots", "FinishDate", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.Bots", "CreateDate", c => c.DateTimeOffset(nullable: false, precision: 7));
        }
    }
}
