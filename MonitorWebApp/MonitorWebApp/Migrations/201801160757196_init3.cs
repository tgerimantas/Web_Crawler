namespace MonitorWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Bots", "CreateDate", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.Bots", "FinishDate", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.Monitors", "MessageDate", c => c.DateTimeOffset(nullable: false, precision: 7));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Monitors", "MessageDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Bots", "FinishDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Bots", "CreateDate", c => c.DateTime(nullable: false));
        }
    }
}
