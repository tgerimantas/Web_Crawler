﻿using System;

namespace MonitorWebApp.Models
{

    public class BotData
    {
        public Guid BotNameId { get; set; }
        public string OperatingSystem { get; set; }
    }

}