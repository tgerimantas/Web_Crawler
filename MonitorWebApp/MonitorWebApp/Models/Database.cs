using System;

namespace MonitorWebApp.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;

    public class Database : DbContext
    {

        public Database()
            : base("name=CrawlerDatabase2")
        {
        }
        public virtual DbSet<Bot> Bots { get; set; }
        public virtual DbSet<StatusType> StatusTypes { get; set; }
        public virtual DbSet<Monitor> Monitor { get; set; }
    }

    public class Bot
    {
        [Key]
        [Required]
        public int Id { get; set; }
        public string BotNameId { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime? CreateDate { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime? FinishDate { get; set; }
        public string OperatingSystem { get; set; }
        [ForeignKey("StatusType")]
        public int StatusTypeId { get; set; }

        public virtual StatusType StatusType { get; set; }
        private IEnumerable<Monitor> MonitorData { get; set; }
    }

    public class StatusType
    {
        [Key]
        [Required]
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Monitor
    {

        [Key]
        [Required]
        public int Id { get; set; }
        [ForeignKey("Bot")]
        public int BotId { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime? MessageDate { get; set; }
        public string InfoType { get; set; }
        public string Info { get; set; }

        public virtual Bot Bot { get; set; }
    }
}