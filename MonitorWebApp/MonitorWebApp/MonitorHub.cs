﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using MonitorWebApp.Models;
using Database = MonitorWebApp.Models.Database;

namespace MonitorWebApp
{ 

    //Status
    //Status = 1 Connected
    //Status = 0 Disconnected

    public class MonitorHub : Hub
    {
        /// <summary>
        /// Register a bot in server
        /// </summary>
        /// <param name="data">bot data</param>
        public async Task Registration(BotData data)
        {
            using (var db = new Database())
            {
                if (data != null)
                {
                    var bot = new Bot()
                    {
                        BotNameId = data.BotNameId.ToString(),
                        OperatingSystem = data.OperatingSystem,
                        CreateDate = DateTime.Now,
                        StatusTypeId = 1 //Connected
                    };

                    db.Bots.Add(bot);
                    await db.SaveChangesAsync();

                    Clients.All.addBotToList(bot);
                }
            }
        }

        /// <summary>
        /// Inform server that seleted bot is shut down
        /// </summary>
        /// <param name="message"></param>
        public async Task Disconnect(BotData message)
        {
            using (var db = new Database())
            {
                var bot = await db.Bots.Where(t => t.BotNameId == message.BotNameId.ToString()).FirstOrDefaultAsync();

                if (bot != null)
                {
                    var finishDate = DateTime.Now;
                    bot.StatusTypeId = 2;
                    bot.FinishDate = finishDate;

                    await db.SaveChangesAsync();

                    var bots = await db.Bots.Where(t => t.StatusTypeId == 1).ToListAsync();

                    Clients.All.updateBotList(bots);

                    Clients.Group(bot.Id.ToString()).addMessage("Bot disconnected", finishDate.ToString("HH:mm:ss"));
                }
            }
        }

        public void Reconnect(BotData message)
        {
   
        }

        /// <summary>
        /// Save command message in database  that bot has been exececuted
        /// </summary>
        /// <param name="message"></param>
        public async Task Send(Message message)
        {
            using (var db = new Database())
            {
                var date = DateTime.Now;

                var bot = await db.Bots.FirstOrDefaultAsync(t => t.BotNameId == message.BotNameId.ToString());

                if (bot != null)
                {
                    var monitorData = new Monitor()
                    {
                        BotId = bot.Id,
                        MessageDate = date,
                        InfoType = message.InfoType,
                        Info = message.Info
                    };

                    db.Monitor.Add(monitorData);
                    await db.SaveChangesAsync();

                    //Send info to display in Web app what command selected bot has been executed    
                    Clients.Group(bot.Id.ToString()).addMessage(message.Info, date.ToString("HH:mm:ss"));
                }
            }
        }

        /// <summary>
        /// Connects user to select bot
        /// </summary>
        /// <param name="id"></param>
        public void Join(string id)
        {
            Groups.Add(Context.ConnectionId, id);
        }

        //Testing with signalr to get data from database
        //-- Very slow 
        public async Task<List<Bot>> GetBots() {

            using (var db = new Database())
            {
                return await db.Bots.ToListAsync();
            }  
        }
    }
}