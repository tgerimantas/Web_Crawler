Simple automatic WEB crawler console app using Selenium and WEB application to display bot actions.

Parts: 
   - Console application written using C# and Selenium library to execute keyboard clicks and execute random web page.
      - Connection between WEB app and console is using a SignalR library. 
      - When we execute a bot app, the bot app sends a request to connect bot app with WEB app. 
      - After that it creates a group that users can connect watch bot actions.
      - Before bot disconnection the bot sends a command to remove group from the list and inform users that bot is disconnected.
     
   - WEB application using MVC 5 and SignalR to communicate between console app and WEB app to display bot actions.
      - Updates real-time a bot list and selected bot actions using SignalR library.
      - All bot data saved in database.
      - Bootstrap library was used to dislay data in WEB app.
